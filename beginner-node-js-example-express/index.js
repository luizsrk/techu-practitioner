const http = require('http')
const express = require('express');

const bodyParser = require('body-parser')
const secrets = require('./secrets')
const mongoose = require('mongoose')

mongoose.connect(secrets.database_uri, { useNewUrlParser: true })

const Schema = mongoose.Schema;


const animalSchema = new Schema({
    name: String,
    pattern: String
})
const Animal = mongoose.model('Animal', animalSchema)

const app = express();

app.get('/', (req, res) => {
    res.json({ succes: true })
})

app.get('/data', (req, res) => {
    res.json({ data: 'Aqui esta tu data test' })
})

app.use(bodyParser.json());

const data = [
    { name: 'cow', pattern: 'patches' },
    { name: 'chetah', pattern: 'sport' },
    { name: 'leopart', pattern: 'sport' },
    { name: 'zeba', pattern: 'spots' },
]

// app.get('/animals',(req,res)=> {
//     res.json({animals:data})
// })

// app.get('/animals' , (req,res) => {
//     if(req.query.pattern){
//         const filteredData = data.filter(animal => {
//             return animal.pattern === req.query.pattern
//         })
//         console.log({animals: filteredData})
//         return res.json({animals: filteredData})
//     }else{
//         res.json({animals:data})
//     }
// })

app.get('/animals', (req, res) => {
    if (req.query.pattern) {
        Animal.find({ pattern: req.query.pattern }, (err, data) => {
            if (err) {
                return res.json({ error: 'Hubo un problema obteniendo animales' });
            }
            console.log('pattern' + { data })
            res.json({ animals: data })
        })
    } else {
        Animal.find({}, (err, data) => {
            if (err) {
                return res.json({ error: 'Hubo un problema obteniendo animales' });
            }
            console.log(JSON.stringify(data))
            res.json({ animals: data });
        })
    }
})

/* app.post('/animals', (req,res,next)=> {    
        const newAnimal = {
        name:req.body.name,
        pattern:req.body.pattern
    }
    data.push(newAnimal)
    res.json(newAnimal)
}) */

app.post('/animals', (req, res, next) => {
    const newAnimal = new Animal({
        name: req.body.name,
        pattern: req.body.pattern
    })
    newAnimal.save((err, animal) => {
        if (err) {
            return res.json({ eror: "Hubo un problema guardando el Animal" })
        }
        res.json(animal);
    })
})

const server = http.createServer(app);
server.listen(3000)
console.log('Servidor habilitado en el puerto 3000')