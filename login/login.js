var usuarios =[];
obtenerDatosusuarios();

function existeUsuario(idValor){
  for(var element of usuarios) {
    var valor = element['numero'];
    if(idValor===valor){
      return true;
    }
  }
  alert("No existe el usuario");
  return false;
}

function validarUsuario(user,pass){
  if(existeUsuario(user) == true){
    validarContraseña(user,pass);
  };
}

function validarContraseña(usuario,contrasenia){
  for(var element of usuarios) {
    var user = element['user'];
    var pass = element['pass'];
    if(usuario===user && contrasenia===pass){
      return true;
    }
  }
  alert("Contraseña incorrecta");
  return false;
}

function obtenerDatosusuarios(){
  usuarios=JSON.parse(localStorage.getItem("usuarios"));
  if(usuarios==null){
    usuarios=[];
  }
}
