var usuarios =[];
obtenerDatosusuarios();
createTableFronJson();
function grabarDato() {
  var usuarioVal = document.getElementById('txtUsuario').value;
  var contraseniaVal = document.getElementById('txtContraseña').value;
  if(existeDato(usuarioVal)==true){
    alert("Ya existe el usuario");
  }else {
    var datosusuarios = {};
    datosusuarios.user = usuarioVal;
    datosusuarios.pass = contraseniaVal;
    usuarios.push(datosusuarios);
    createTableFronJson();
    localStorage.setItem("usuarios", JSON.stringify(usuarios));
  }
}

function obtenerDatosusuarios(){
  usuarios=JSON.parse(localStorage.getItem("usuarios"));
  if(usuarios==null){
    usuarios=[];
  }
}

function createTableFronJson(){

  var col = [];
  for (var i=0 ;i<usuarios.length;i++){
    for (var key in usuarios[i]){
      if(col.indexOf(key)=== -1){
        col.push(key);
      }
    }
  }
  var table = document.createElement("table");
  var tr = table.insertRow(-1);                   // TABLE ROW.
  for (var i = 0; i < col.length; i++) {
    var th = document.createElement("th");      // TABLE HEADER.
    th.innerHTML = col[i].toUpperCase();
    tr.appendChild(th);
  }

  for (var i = 0; i < usuarios.length; i++) {
    tr = table.insertRow(-1);
    for (var j = 0; j < col.length; j++) {
      var tabCell = tr.insertCell(-1);
      tabCell.innerHTML = usuarios[i][col[j]];
    }
  }

  var divContainer = document.getElementById("mostrarData");
  divContainer.innerHTML = "";
  divContainer.appendChild(table);

}
function eliminarDato (idValor){
  usuarios.forEach(function(element, index) {
    if(idValor===element.numero){
      usuarios.splice(index, 1);
    }
  });
  createTableFronJson();
  localStorage.setItem("usuarios", JSON.stringify(usuarios));

}
function eliminarAll(){
  usuarios =[];
  localStorage.clear();
  createTableFronJson();
}
function existeDato(idValor){
  for(var element of usuarios) {
    var valor = element['numero'];
    if(idValor===valor){
      return true;
    }
  }
  return false;
}
