let express = require('express')
let router = express.Router();

// QueryString => query property on the request object
// localhost:3000/person?name=kevin&age35
router.get('/person', (req, res) => {
  if(req.query.name){
    res.send('Haz solicitado una persona con el nombre '+ req.query.name)
  }else {
    res.send('Haz solicitado una persona')
  }

});

// Params property on the request object
//localhost:3000/person/kevin
router.get ('/person/:name' ,(req, res) =>{
  res.send('Haz solicitado una persona con el nombre '+ req.params.name)
});

module.exports =router
