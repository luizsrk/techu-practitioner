const animals = [
    {name: 'cat', sound:'mowo'},
    {name: 'dog', sound:'bark'},
    {name: 'mouse', sound:'squek'}
];

animals.forEach(animal => {
    console.log(`The ${animal.name} goes ${animal.sound}`);
});