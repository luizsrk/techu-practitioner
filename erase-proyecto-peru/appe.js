var express = require ('express');
var userFile = require ('./user.json');
var _ = require("underscore");
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var port = process.env.PORT || 8081;
var app = express();
var URLbase = '/apiperu/v1/';
app.use(bodyParser.json());

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdapi_peru/collections/"
var apiKeyMlab = "apiKey=8UTZVy2FCsLnVPizP9tX4tl3HEHWiQvM"

var clienteMlab;

//Login
app.post ('/bdapi_peru/v1/login',(req,res,next) =>{
  console.log('Login POST')
  var email = req.headers.email
  var password = req.headers.password
  var query = 'q={"email":"'+ email + '","password":"'+ password + '"}'  
  clienteMlab = requestJson.createClient(urlMlabRaiz + "user?" + query + "&I=1&" + apiKeyMlab)  
  clienteMlab.get('', (err,resM,body) =>{
    if(!err){
      if(body.length == 1){//Login ok
        clienteMlab = requestJson.createClient(urlMlabRaiz + "user")
        var cambio = '{"$set":{"logged":true}}'
        clienteMlab.put ('?q={"userID": ' +body[0].userID +'}&' + apiKeyMlab, JSON.parse(cambio) ,(errP, resP, bodyP) =>{
          console.log(body[0].userID)
          res.send({"login":"ok" , "id":body[0].userID, "nombre":body[0].first_name, "apellidos":body[0].last_name})

        })
    }else{
      res.status(404).json('Usuario no encontrado')
    }
  }
  })
 })

 //Login
app.post ('/bdapi_peru/v2/login',(req,res,next) =>{
  console.log('Login POST')
  var email = req.headers.email
  var password = req.headers.password
  var query = 'q={"email":"'+ email + '","password":"'+ password + '"}'  
  clienteMlab = requestJson.createClient(urlMlabRaiz + "user?" + query + "&I=1&" + apiKeyMlab)  
  clienteMlab.get('', (err,resM,body) =>{
    if(!err){
      if(body.length == 1){//Login ok
        clienteMlab = requestJson.createClient(urlMlabRaiz + "user")
        var cambio = '{"$set":{"logged":true}}'
        clienteMlab.put ('?q={"userID": ' +body[0].userID +'}&' + apiKeyMlab, JSON.parse(cambio) ,(errP, resP, bodyP) =>{
          console.log(body[0].userID)
          res.send({"login":"ok" , "id":body[0].userID, "nombre":body[0].first_name, "apellidos":body[0].last_name})

        })
    }else{
      res.status(404).json('Usuario no encontrado')
    }
  }
  })
 })

 //Logout
app.post ('/bdapi_peru/v1/logout',(req,res,next) =>{
  console.log('Logout POST')
  var userID = req.headers.userid  
  var query = 'q={"userID":'+ userID + ',"logged":true}' 
  clienteMlab = requestJson.createClient(urlMlabRaiz + "user?" + query + "&I=1&" + apiKeyMlab)  
  clienteMlab.get('', (err,resM,body) =>{
    if(!err){
      if(body.length == 1){//Estaba logueado
        clienteMlab = requestJson.createClient(urlMlabRaiz + "user")
        var cambio = '{"$set":{"logged":false}}'
        clienteMlab.put ('?q={"userID": ' +body[0].userID +'}&' + apiKeyMlab, JSON.parse(cambio) ,(errP, resP, bodyP) =>{
          
          res.send({"logout":"ok" , "id":body[0].userID, "nombre":body[0].first_name, "apellidos":body[0].last_name})

        })
    }else{
      res.status(200).json('Usuario no logueado previamente')
    }
  }
  })
 })

  //Logout
app.post ('/bdapi_peru/v2/logout',(req,res,next) =>{
  console.log('Logout POST')
  var userID = req.headers.userid  
  var query = 'q={"userID":'+ userID + ',"logged":true}' 
  clienteMlab = requestJson.createClient(urlMlabRaiz + "user?" + query + "&I=1&" + apiKeyMlab)  
  clienteMlab.get('', (err,resM,body) =>{
    if(!err){
      if(body.length == 1){//Estaba logueado
        clienteMlab = requestJson.createClient(urlMlabRaiz + "user")
        var cambio = '{"$set":{"logged":false}}'
        clienteMlab.put ('?q={"userID": ' +body[0].userID +'}&' + apiKeyMlab, JSON.parse(cambio) ,(errP, resP, bodyP) =>{
          
          res.send({"logout":"ok" , "id":body[0].userID, "nombre":body[0].first_name, "apellidos":body[0].last_name})

        })
    }else{
      res.status(200).json('Usuario no logueado previamente')
    }
  }
  })
 })


//Get user
app.get('/bdapi_peru/v1/user/', function (req,res){
    console.log('/bdapi_peru/v1/user');
    clienteMlab = requestJson.createClient(urlMlabRaiz + "user?" +apiKeyMlab);
    console.log(urlMlabRaiz + "/user?" +apiKeyMlab);
    clienteMlab.get('', function (err, responseMlab,body){
      var response = {};
      if(err){
        response = {
          "msg":"Error obtener usuario"
        }
        res.status(500);
      }else{
        if(body.length>0){
          response = body;
        }else{
          response = {
            "msg":"Ningun elemento User"
          }
          res.status(404);
        }
      }
      res.send(response);
    });

});

//Add User
app.post('/bdapi_peru/v1/user/', function (req,res){
  console.log('POST /bdapi_peru/v1/user');    
  var newUser =
  { "userID": req.body.userID,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "gender": req.body.gender,
    "ip_address": req.body.ip_address,
    "password": req.body.password
  }
  
  console.log(urlMlabRaiz + "user?"+apiKeyMlab,JSON.stringify(newUser));
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?"+apiKeyMlab);
  clienteMlab.post('',newUser, function(err,responseMlab,body){
  var response = {};  
    if(err){
      response = {
        "msg":"Error obtener usuario"
      }
      res.status(500);
    }else{      
        response = JSON.stringify(newUser);
    res.send(response);
    }
  })
})

//Update User
app.put('/bdapi_peru/v1/user/:id', function (req,res){
  console.log('POST /bdapi_peru/v1/user'+ req.params.id);  
  let idUser= req.params.id;
  let queryStringID = 'q={"userID":' + idUser + '}&';
  var updateUser =  { 
    "userID": req.body.userID,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "gender": req.body.gender,
    "ip_address": req.body.ip_address,
    "password": req.body.password
  }  
  
  clienteMlab = requestJson.createClient(urlMlabRaiz+ "user?"+queryStringID +apiKeyMlab);
  clienteMlab.get('',function(error, respuestaMLab , body) {
    console.log('url_mlab '+urlMlabRaiz + "/user?"+ queryStringID+apiKeyMlab);
     console.log("body_get:"+ JSON.stringify(body));
      if(error){
        response = {
          "msg":"Error obtener usuario"
        }
        res.status(500);
      }else{
        if(body.length>0){
          response = body;
        }else{
          response = {
            "msg":"Ningun elemento User"
          }
          res.status(404);
        }
      }

     var cambio = '{"$set":' + JSON.stringify(updateUser) + '}';
     console.log('cambio '+cambio)
     clienteMlab.put(urlMlabRaiz + 'user?q={"userID": ' + idUser + '}&' + apiKeyMlab, JSON.parse(cambio),
      function(error, respuestaMLab, body_update) {
       console.log("put_body:"+ JSON.stringify(body_update));
       res.send(body);
      });
 });
  
})

//DELETE user with id
app.delete('/bdapi_peru/v1/user/:id',
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"userID":' + id + '}&';
    console.log(urlMlabRaiz + 'user?' + queryStringID + apiKeyMlab);
    var httpClient = requestJson.createClient(urlMlabRaiz);
    httpClient.get('user?' +  queryStringID + apiKeyMlab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        console.log("body delete:"+ respuesta);
        httpClient.delete(urlMlabRaiz + "user/" + respuesta._id.$oid +'?'+ apiKeyMlab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });


//Get user
app.get(URLbase + 'users', function (req,res){
    console.log('GET ' +URLbase+ 'users ' + req.params);
    res.send(userFile);
});

app.get(URLbase + 'users', function (req,res){
    console.log(req.query);
    console.log(req.query.id);
    filtered = _.where(userFile, {userID: parseInt(req.query.id)});
    res.status(200);
    res.send(filtered);

});

//GET user by id

app.get(URLbase + 'users/:id', function (req,res){
    console.log('GET ' +URLbase+ 'users ' + req.params.id);
    var filtered = _.where(userFile, {userID: parseInt(req.params.id)});
    if (filtered !== undefined){
      res.status(200);
      res.send(filtered);
    }
});

//POST add user
app.post(URLbase + 'user', function (req,res){
    console.log('POST ' +URLbase+ 'add/user/ ');
    let newID = userFile.length+1;
    let newUser =  {
      "userID": newID,
      "first_name": "Josmil",
      "last_name": "Tito",
      "email": "josmil.tito@bbva.com",
      "gender": "Male",
      "ip_address": "149.182.234.123",
      "password": "asdfas345"
    }
    userFile.push(newUser);
    res.status(200);
    res.send({"msg":"Usuarios Añadido",newUser});
});

//DELETE add user
app.delete(URLbase + 'users/:id', function (req,res){
    console.log('DELETE ' +URLbase+ 'users ' + req.params.id);
    var id = parseInt(req.params.id);
    userFile.forEach(function(element, index) {
      if(id===element.userID){
        userFile.splice(index, 1);
      }else{
        res.status(204);
        res.send({"msg":"No existe usuario"});
      }
    });
    res.send({"msg":"Usuarios Eliminado",id});
});

//MODIFY  user
app.put(URLbase + 'users/:id', function (req,res){
    console.log('PUT ' +URLbase+ 'users ' + req.params.id);
    let id = parseInt(req.params.id);
    userFile.forEach(function(element, index) {
      if(id===element.userID){
        userFile.splice(index, 1);
      } else{
      res.status(404);
      res.send({"msg":"No existe usuario"});
    }
      });
      let newUser =  {
        "userID": id ,
        "first_name": "Josmil",
        "last_name": "Tito",
        "email": "josmil.tito@bbva.com",
        "gender": "Male",
        "ip_address": "149.182.234.123",
        "password": "asdfas345"
      }
      userFile.push(newUser);
      // userFile.forEach(function(element, index){
      // if(id===index.userID){
      //   userFile[index].userID = newID,
      //   userFile[index].first_name= "Josmil",
      //   userFile[index].last_name= "Tito choquevica",
      //   userFile[index].email= "josmil.tito@bbva.com",
      //   userFile[index].gender= "Male",
      //   userFile[index].ip_address= "149.182.234.123",
      //   userFile[index].password= "asdfas345"
      // }
      // });
      // for(var element in userFile) {
      //   if(id===userFile[element].userID){
      //     userFile[element].first_name= "Josmil",
      //     userFile[element].last_name= "Tito choquevica",
      //     userFile[element].email= "josmil.tito@bbva.com",
      //     userFile[element].gender= "Male",
      //     userFile[element].ip_address= "149.182.234.123",
      //     userFile[element].password= "asdfas345"
      //     break;
      //   }
      // }
    res.status(200);
    res.send({"msg":"Usuarios Actualizado",id});
});


app.get('/', function (req , res){
  res.send('Hello World!');
});

app.get('/josmil' , function (req , res){
  res.send("Hola Josmil tito Ch df g");

});
app.listen(3000);
